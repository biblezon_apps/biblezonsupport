package com.biblezon.support.webservice;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.support.application.ApplicationController;
import com.biblezon.support.global.GlobalHelper;
import com.biblezon.support.utils.AndroidAppUtils;
import com.biblezon.support.webservice.control.WebserviceAPIErrorHandler;
import com.biblezon.support.webservice.control.WebserviceAPISuccessFailManager;
import com.biblezon.support.webservice.ihelper.WebAPIResponseListener;

import org.json.JSONObject;

/**
 * check version of application
 *
 * @author Shruti
 */
public class VersionCheckAPIHandler {
    DownloadManager downloadManager;
    private Activity mActivity;
    private Context context;
    /**
     * Debug TAG
     */
    private String TAG = VersionCheckAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    private long downloadReference;
    // broadcast receiver to get notification about ongoing downloads
    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            // check if the broadcast message is for our Enqueued download
            long referenceId = intent.getLongExtra(
                    DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            if (downloadReference == referenceId) {

                Log.v("VersionCheckAPIHandler",
                        "Downloading of the new app version complete");
                // start the installation of the latest version
                Intent installIntent = new Intent(Intent.ACTION_VIEW);
                installIntent.setDataAndType(downloadManager
                                .getUriForDownloadedFile(downloadReference),
                        "application/vnd.android.package-archive");
                installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(installIntent);

            }
        }
    };

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public VersionCheckAPIHandler(Activity mActivity,
                                  WebAPIResponseListener webAPIResponseListener) {
        // AndroidAppUtils.showProgressDialog(mActivity, "Loading...", false);
        this.mActivity = mActivity;
        this.context = mActivity;
        this.mResponseListener = webAPIResponseListener;
        IntentFilter filter = new IntentFilter(
                DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        mActivity.registerReceiver(downloadReceiver, filter);
        postAPICall();

    }

    /**
     * Making json object request
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        String version_url = (GlobalHelper.VERSION_CHECK_BASEURL + GlobalHelper.APK_NAME)
                .toLowerCase().trim();
        AndroidAppUtils.showLog(TAG, "version_url : " + version_url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                version_url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                AndroidAppUtils.showInfoLog(TAG, "Response :"
                        + response);
                parseAPIResponse(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                WebserviceAPIErrorHandler.getInstance()
                        .VolleyErrorHandlerReturningString(error, mActivity);
//						AndroidAppUtils.hideProgressDialog();
                mResponseListener.onSuccessResponse();
            }
        }) {

        };

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(jsonObjReq,
                GlobalHelper.VERSION_CHECK_KEY);
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(20,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // MassAppApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalHelper.MASS_ACTIVITY_API);
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(JSONObject response) {
        if (WebserviceAPISuccessFailManager.getInstance().checkVersionResponseCode(
                response)) {
            /* Success of API Response */
            try {
                float mCurrentVersion = 0f, mLatestVersion = 0f;
                try {
                    mLatestVersion = Float.parseFloat(response.getString(GlobalHelper.LATEST_VERSION));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                final String appURI = response
                        .getString(GlobalHelper.UPDATED_APP_URL);
                PackageInfo pInfo = null;
                try {
                    pInfo = mActivity.getPackageManager().getPackageInfo(
                            mActivity.getPackageName(), 0);
                } catch (NameNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    mCurrentVersion = Float.parseFloat(pInfo.versionName);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (mLatestVersion > mCurrentVersion) {
                    // oh yeah we do need an upgrade, let the user know send
                    // an alert message
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            mActivity);
                    builder.setMessage(
                            "There is newer version of this application available, click OK to upgrade now?")
                            .setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        // if the user agrees to upgrade
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            // start downloading the file
                                            // using the download manager
                                            downloadManager = (DownloadManager) mActivity
                                                    .getSystemService(Context.DOWNLOAD_SERVICE);
                                            Uri Download_Uri = Uri
                                                    .parse(appURI);
                                            DownloadManager.Request request = new DownloadManager.Request(
                                                    Download_Uri);
                                            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
                                            request.setAllowedOverRoaming(false);
                                            request.setTitle("My Andorid App Download");
                                            request.setDestinationInExternalFilesDir(
                                                    mActivity,
                                                    Environment.DIRECTORY_DOWNLOADS,
                                                    GlobalHelper.APK_NAME
                                                            + ".apk");
                                            downloadReference = downloadManager
                                                    .enqueue(request);
                                        }
                                    });
                    // show the alert message
                    builder.create().show();
                } else {
                    mResponseListener.onSuccessResponse();
                }

            } catch (Exception e) {
                e.printStackTrace();
                mResponseListener.onSuccessResponse();
            }

        } else {
            /* Fail of API Response */
            // AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
            // "Something Went wrong !!", null);
            mResponseListener.onSuccessResponse();
        }
    }
}
