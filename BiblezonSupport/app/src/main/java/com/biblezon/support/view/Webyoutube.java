package com.biblezon.support.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.VideoView;

import com.biblezon.support.R;

/**
 * Created by Diseyi on 6/24/2016.
 */
public class Webyoutube extends Activity {


    // Put in your Video URL here
    private String videoUrl;
    // Declare some variables
    private ProgressDialog pDialog;
    VideoView videoView;
    WebView displayYoutubeVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_activity);
        String youtubeID = getIntent().getExtras().get("youtubeID").toString();
        findViewById(R.id.dummyView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        findViewById(R.id.closeView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayYoutubeVideo.destroy();
                finish();
            }
        });
        String frameVideo = "https://www.youtube.com/embed/" + youtubeID + "?autoplay=1";
        final ProgressDialog pd = ProgressDialog.show(Webyoutube.this, "", "Please wait, loading your video.", true);

        displayYoutubeVideo = (WebView) findViewById(R.id.webview);
        displayYoutubeVideo.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                pd.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                pd.dismiss();
            }
        });
        WebSettings webSettings = displayYoutubeVideo.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        displayYoutubeVideo.loadUrl(frameVideo);


    }

}
