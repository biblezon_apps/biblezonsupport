package com.biblezon.support.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.biblezon.support.R;

import org.json.JSONException;
import org.json.JSONObject;

public class FeedBackDetailActivity extends AppCompatActivity {

    SharedPreferences prefs;
    TextView title, myFeedback, answer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back_detail);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        title = (TextView) findViewById(R.id.headerName);
        myFeedback = (TextView) findViewById(R.id.txt_my_feedback);
        answer = (TextView) findViewById(R.id.txt_reply);
        title.setText("Feedback Detail");

        Intent intent = getIntent();
        if (intent.hasExtra("data")){
            try {
                JSONObject obj = new JSONObject(intent.getStringExtra("data"));
                if (obj != null){
                    myFeedback.setText(obj.getString("description"));
                    if (obj.getInt("is_answered") == 0){
                        answer.setText("Not answered yet.");
                    }else{
                        JSONObject jsonObject = obj.getJSONObject("answer");
                        answer.setText(jsonObject.getString("answer"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
