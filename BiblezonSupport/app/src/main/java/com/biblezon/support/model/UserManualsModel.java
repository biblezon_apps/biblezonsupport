package com.biblezon.support.model;

/**
 * Created by Anshuman on 12/16/2015.
 */
public class UserManualsModel {

    String id, description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
