package com.biblezon.support.view;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.biblezon.support.R;
import com.biblezon.support.ihelper.AlertDialogClickListener;
import com.biblezon.support.utils.AndroidAppUtils;
import com.biblezon.support.utils.AppDialogUtils;
import com.biblezon.support.webservice.FeedbackAPIHandler;
import com.biblezon.support.webservice.ihelper.WebAPIResponseListener;


/**
 * Slider fragment Help Screen
 *
 * @author Anshuman
 */
public class FeedbackScreen extends Fragment {

    /**
     * View fields instances
     */
    @SuppressWarnings("unused")
    private String TAG = FeedbackScreen.class.getSimpleName();
    private Activity mActivity;
    /**
     * Screen base view
     */
    private View mView;
    /**
     * Screen Views
     */
    private EditText nameEdit, emailEditText, msgEditText, mobileNumberEditText;
    private Button sendMessageButton;
    private TextView headerName;
    private SharedPreferences prefs;
    private ImageView actionList;

    /*
     * (non-Javadoc)
     *
     * @see
     * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.feedback_screen, container, false);
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        initViews();
        assignClick();
        return mView;
    }

    /**
     * initializing view fields
     */
    private void initViews() {
        mActivity = getActivity();
        nameEdit = (EditText) mView.findViewById(R.id.nameEdit);
        emailEditText = (EditText) mView.findViewById(R.id.emailEditText);
        msgEditText = (EditText) mView.findViewById(R.id.msgEditText);
        mobileNumberEditText = (EditText) mView.findViewById(R.id.mobileNumberEditText);
        sendMessageButton = (Button) mView.findViewById(R.id.sendMessageButton);
        headerName = (TextView) mView.findViewById(R.id.headerName);
        headerName.setText(mActivity.getResources().getString(R.string.feedback));
        actionList = (ImageView) mView.findViewById(R.id.action_list);

        nameEdit.setText(prefs.getString("name",""));
        emailEditText.setText(prefs.getString("email",""));
    }


    /**
     * Assign Click
     */
    private void assignClick() {
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValiadtion()) {
                    new FeedbackAPIHandler(mActivity, mResponseListener(), nameEdit.getText().toString(), emailEditText.getText().toString(), msgEditText.getText().toString(), mobileNumberEditText.getText().toString(), prefs.getString("token",""), true);
                }

            }
        });

        actionList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), FeebBackListActivity.class));
            }
        });
    }

    private WebAPIResponseListener mResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {
            @Override
            public void onSuccessResponse(Object... arguments) {
                AppDialogUtils.showAlertDialog(mActivity, "Your feedback has been successfully submitted.", "Ok", onDialogClick());
            }

            @Override
            public void onFailResponse(Object... arguments) {
                AppDialogUtils.showAlertDialog(mActivity, "Your feedback has been successfully submitted.", "Ok", onDialogClick());
            }

            @Override
            public void onOfflineResponse(Object... arguments) {
                AppDialogUtils.showAlertDialog(mActivity, "Your feedback has been successfully submitted.", "Ok", onDialogClick());
            }
        };
        return mListener;
    }

    /**
     * on Success of password chnage
     *
     * @return
     */
    private AlertDialogClickListener onDialogClick() {
        AlertDialogClickListener mListener = new AlertDialogClickListener() {

            @Override
            public void onClickOfAlertDialogPositive() {
                // TODO Auto-generated method stub
//                nameEdit.setText("");
//                emailEditText.setText("");
                msgEditText.setText("");
                mobileNumberEditText.setText("");
            }
        };
        return mListener;
    }

    private boolean checkValiadtion() {
        if (!(nameEdit.getText().length() > 0)) {
            AndroidAppUtils.showToast(mActivity, "Please enter your name.");
            return false;
        } else if (!(emailEditText.getText().length() > 0)) {
            AndroidAppUtils.showToast(mActivity, "Please enter your email address.");
            return false;
        } else if (!(AndroidAppUtils.isEmailIDValidate(emailEditText.getText().toString()))) {
            AndroidAppUtils.showToast(mActivity, "Please enter valid email address.");
            return false;
        } else if (!(mobileNumberEditText.getText().length() > 0)) {
            AndroidAppUtils.showToast(mActivity, "Please enter your phone number.");
            return false;
        } else if (!(AndroidAppUtils.isMobileValid(mobileNumberEditText.getText().toString()))) {
            AndroidAppUtils.showToast(mActivity, "Please enter valid phone number.");
            return false;
        } else if (!(msgEditText.getText().length() > 0)) {
            AndroidAppUtils.showToast(mActivity, "Please enter your message.");
            return false;
        } else
            return true;
    }


}
