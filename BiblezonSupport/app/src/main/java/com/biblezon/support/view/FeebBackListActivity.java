package com.biblezon.support.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.biblezon.support.R;
import com.biblezon.support.adapter.JSONArrayAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FeebBackListActivity extends AppCompatActivity {

    SharedPreferences prefs;
    TextView title;
    ListView listView;
    JSONArrayAdapter arrayAdapter;

    String FEEDBACK_URL = "http://biblezon.com/appapicms/webservices/support_feedbacks";
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeb_back_list);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        title = (TextView) findViewById(R.id.headerName);
        listView = (ListView) findViewById(R.id.list_view);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    if (arrayAdapter != null){
                        JSONObject obj = (JSONObject) arrayAdapter.getItem(i);
                        startActivity(new Intent(FeebBackListActivity.this, FeedBackDetailActivity.class)
                        .putExtra("data", obj.toString()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        title.setText("My FeedBacks");
        fetchFeedBacks();
    }


    private void fetchFeedBacks() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, FEEDBACK_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        setLoadingBar(false);
                        Log.d(getClass().getSimpleName(),"response= " + response);
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            String isSuccess = jsonObject.optString("replyCode");
                            String replymsg = jsonObject.optString("replyMsg");
                            if (isSuccess.equals("200") || isSuccess.equals("success")) {
                                arrayAdapter = new JSONArrayAdapter(FeebBackListActivity.this,jsonObject.getJSONArray("data"),
                                        android.R.layout.simple_list_item_2,new String[]{"description"},new int[]{android.R.id.text1}){
                                    @Override
                                    public View getView(int position, View convertView, ViewGroup parent) {
                                        View view = super.getView(position, convertView, parent);
                                        view.setBackgroundColor(Color.WHITE);
                                        JSONObject obj = (JSONObject) getItem(position);
                                        TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                                        try {
                                            text2.setText(obj.getInt("is_answered") == 1 ? "Please tap here to see response from Biblezon Team" : "Not answered yet");
                                        } catch (JSONException e) {
                                            text2.setText("");
                                        }
                                        return view;
                                    }
                                };
                                listView.setAdapter(arrayAdapter);


                            } else {
                                Toast.makeText(FeebBackListActivity.this, replymsg, Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        setLoadingBar(false);
                        Toast.makeText(FeebBackListActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("token",prefs.getString("token",""));
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        setLoadingBar(true);
    }

    public void setLoadingBar(boolean enable){
        if (enable){
            if (pDialog == null){
                pDialog = ProgressDialog.show(this,null,"Loading, please wait..");
            }
        }else {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
                pDialog = null;
            }
        }
    }
}
