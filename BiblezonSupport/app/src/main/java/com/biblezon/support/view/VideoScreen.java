package com.biblezon.support.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.biblezon.support.R;
import com.biblezon.support.adapter.VideoListAdapter;
import com.biblezon.support.utils.AndroidAppUtils;
import com.biblezon.support.view.youtube.OpenYouTubePlayerActivity;
import com.biblezon.support.webservice.SupportAPIHandler;


/**
 * Slider fragment Help Screen
 *
 * @author Anshuman
 */
public class VideoScreen extends Fragment {

    public static String youTub_URL = "", videoName = "";
    /**
     * View fields instances
     */
    @SuppressWarnings("unused")
    private String TAG = VideoScreen.class.getSimpleName();
    private Activity mActivity;
    /**
     * Screen base view
     */
    private View mView;
    /**
     * Screen Views
     */
    private ListView videolist;
    /**
     * Video List Adapter
     */
    private VideoListAdapter mVideoListAdapter;
    private TextView headerName;

    /**
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */

    /*
     * (non-Javadoc)
     *
     * @see
     * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.video_screen, container, false);
        initViews();
        assignClick();
        return mView;
    }

    /**
     * Assign Click
     */
    private void assignClick() {
        videolist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                youTub_URL = SupportAPIHandler.mListOfVideos.get(position).getLink();
                videoName = SupportAPIHandler.mListOfVideos.get(position).getTitle();
//                startActivity(new Intent(mActivity, VideoPlayer.class));

             //   Intent lVideoIntent = new Intent(null, Uri.parse("ytv://" + AndroidAppUtils.getVideoId(SupportAPIHandler.mListOfVideos.get(position).getLink())), mActivity, OpenYouTubePlayerActivity.class);
              //  startActivity(lVideoIntent);
              String VideoId = AndroidAppUtils.getVideoId(youTub_URL);

                Intent lVideoIntent = new Intent(mActivity, Webyoutube.class);
                lVideoIntent.putExtra("youtubeID",VideoId  );
                startActivity(lVideoIntent);
//                startActivity(new Intent(mActivity, YoutubeVideoScreen.class));
//                startActivity(new Intent(Intent.ACTION_VIEW,
//                        Uri.parse(URL)));
            }
        });
    }

    /**
     * initializing view fields
     */
    private void initViews() {
        mActivity = getActivity();
        videolist = (ListView) mView.findViewById(R.id.videolist);
        headerName = (TextView) mView.findViewById(R.id.headerName);
        headerName.setText(mActivity.getResources().getString(R.string.video));
        mVideoListAdapter = new VideoListAdapter(mActivity);
        videolist.setAdapter(mVideoListAdapter);
        mVideoListAdapter.addUpdatedData(SupportAPIHandler.mListOfVideos);
        mVideoListAdapter.notifyDataSetChanged();

    }

//    startActivity(new Intent(Intent.ACTION_VIEW,
//                  Uri.parse("http://www.youtube.com/watch?v=...")));


}
