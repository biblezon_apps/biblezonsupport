package com.biblezon.support.view;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.biblezon.support.R;
import com.biblezon.support.webservice.SupportAPIHandler;


/**
 * Slider fragment UserManualScreen Screen
 *
 * @author Anshuman
 */
public class UserManualScreen extends Fragment {

    /**
     * View fields instances
     */
    @SuppressWarnings("unused")
    private String TAG = UserManualScreen.class.getSimpleName();
    private Activity mActivity;
    /**
     * Screen base view
     */
    private View mView;
    private TextView headerName;
    //    private ListView usermanuallist;
//    private UserManualListAdapter userManualListAdapter;
    private WebView userManualWebView;

    /**
     * Screen Views
     */

    /*
     * (non-Javadoc)
     *
     * @see
     * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.user_manual_screen, container, false);
        initViews();
        return mView;
    }

    /**
     * initializing view fields
     */
    private void initViews() {
        mActivity = getActivity();
        headerName = (TextView) mView.findViewById(R.id.headerName);
        headerName.setText(mActivity.getResources().getString(R.string.user_manual));
//        usermanuallist = (ListView) mView.findViewById(R.id.usermanuallist);
//        /*Adapter*/
//        userManualListAdapter = new UserManualListAdapter(mActivity);
//        usermanuallist.setAdapter(userManualListAdapter);
//        userManualListAdapter.addUpdatedData(SupportAPIHandler.mListOfUserManuals);
//        userManualListAdapter.notifyDataSetChanged();

        userManualWebView = (WebView) mView.findViewById(R.id.userManualWebView);
        userManualWebView.setBackgroundColor(Color.TRANSPARENT);
        userManualWebView.loadDataWithBaseURL("", "<font color='black'>" + "<font size='4'>"
                + SupportAPIHandler.mListOfUserManuals.get(0).getDescription()
                + "</font>", "text/html", "UTF-8", "");
    }


}
