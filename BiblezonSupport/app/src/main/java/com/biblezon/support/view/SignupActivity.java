package com.biblezon.support.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.biblezon.support.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    private android.widget.EditText nameEditSignup;
    private android.widget.EditText emailEditTextSignup;
    private android.widget.EditText mobileNumberEditTextSignup;
    private android.widget.EditText passwordEditTextSignup;
    private android.widget.Button signupButtonSignup;

    private String Register_Url = "http://biblezon.com/appapicms/webservices/support_register";

    private static final String TAG = "SignupActivity";
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        this.signupButtonSignup = (Button) findViewById(R.id.signupButton_Signup);
        this.passwordEditTextSignup = (EditText) findViewById(R.id.passwordEditText_Signup);
        this.mobileNumberEditTextSignup = (EditText) findViewById(R.id.mobileNumberEditText_Signup);
        this.emailEditTextSignup = (EditText) findViewById(R.id.emailEditText_Signup);
        this.nameEditSignup = (EditText) findViewById(R.id.nameEdit_Signup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Sign Up");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        prefs = PreferenceManager.getDefaultSharedPreferences(SignupActivity.this);

        signupButtonSignup.setOnClickListener(this);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    public void signup(final String email, final String pass, final String contact, final String name) {

        //Implementation of volley library for server task
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Register_Url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.i(TAG, "onResponse: " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.optString("replycode");
                            String msg = jsonObject.optString("replymsg");

                            Log.i(TAG, "onResponse: " + success + "  " + msg);

                            if (success.equals("200")) {

                                String token = jsonObject.optString("token");

                                prefs.edit().putBoolean("Is_Login", true).apply();
                                prefs.edit().putString("token", token).apply();
                                
                                Log.d(TAG, "onResponse() " + token);

                                Toast.makeText(SignupActivity.this, msg, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(SignupActivity.this,SupportScreen.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP));

                            } else {

                                Toast.makeText(SignupActivity.this, msg, Toast.LENGTH_LONG).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(SignupActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("email", email);
                params.put("phone", contact);
                params.put("password", pass);
                params.put("device_token", "");
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.signupButton_Signup:

                String pass = passwordEditTextSignup.getText().toString().trim();
                String name = nameEditSignup.getText().toString().trim();
                String email = emailEditTextSignup.getText().toString().trim();
                String contact = mobileNumberEditTextSignup.getText().toString().trim();

                if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(pass) && !TextUtils.isEmpty(contact)) {

                    signup(email, pass, contact, name);

                }

                break;


        }
    }
}
