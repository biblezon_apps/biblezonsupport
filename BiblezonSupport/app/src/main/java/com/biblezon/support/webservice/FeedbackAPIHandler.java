package com.biblezon.support.webservice;

import android.app.Activity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.biblezon.support.application.ApplicationController;
import com.biblezon.support.global.GlobalHelper;
import com.biblezon.support.ihelper.AlertDialogClickListener;
import com.biblezon.support.utils.AndroidAppUtils;
import com.biblezon.support.webservice.control.WebserviceAPIErrorHandler;
import com.biblezon.support.webservice.ihelper.WebAPIResponseListener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Feedback API Handler
 *
 * @author Anshuman
 */
public class FeedbackAPIHandler {
    private final String token;
    /**
     * Instance object of Login API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = FeedbackAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    /**
     * API Request Param
     */
    private String name = "", email = "", message = "", phone = "";

    /**
     * @param mActivity
     * @param webAPIResponseListener
     * @param mActivity
     */

    public FeedbackAPIHandler(Activity mActivity,
                              WebAPIResponseListener webAPIResponseListener, String userName,
                              String email, String message, String phone,String token,
                              boolean isProgressShowing) {
        if (isProgressShowing) {
            AndroidAppUtils.showProgressDialog(mActivity, "Sending...", false);
        }
        this.mActivity = mActivity;
        this.mResponseListener = webAPIResponseListener;
        this.name = userName;
        this.email = email;
        this.phone = phone;
        this.message = message;
        this.token = token;
        postAPICallString();
    }

//    /**
//     * JSON Request to user
//     *
//     * @return
//     */
//    private JSONObject createJSONRequest() {
//        try {
//            JSONObject mRequestJsonObject = new JSONObject();
//
//            mRequestJsonObject.put("name", name);
//            mRequestJsonObject.put("email", email);
//            mRequestJsonObject.put("description", message);
//            mRequestJsonObject.put("phone", phone);
//            return mRequestJsonObject;
//        } catch (Exception e) {
//            // TODO: handle exception
//            e.printStackTrace();
//        }
//        return null;
//
//    }


    /**
     * Making String object request
     */
    public void postAPICallString() {
        StringRequest strReq = new StringRequest(Method.POST,
                GlobalHelper.FEEDBACK_API.trim(), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                AndroidAppUtils.showInfoLog(TAG, "Response :"
                        + response);
                mResponseListener.onSuccessResponse();
                AndroidAppUtils.hideProgressDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                AndroidAppUtils.showErrorLog(
                        TAG,
                        WebserviceAPIErrorHandler.getInstance()
                                .VolleyErrorHandlerReturningString(
                                        error, mActivity));
                mResponseListener.onOfflineResponse();
                AndroidAppUtils.hideProgressDialog();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("email", email);
                params.put("description", message);
                params.put("phone", phone);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("token",token);
                return map;
            }
        };

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(strReq, GlobalHelper.FEEDBACK_KEY);
        // set request time-out
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                GlobalHelper.ONE_SECOND * GlobalHelper.API_REQUEST_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // ApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.CHANGE_KEY);
    }

    /**
     * Parse SignUp Response
     *
     * @param response
     */
    protected void parseChangePassAPIResponse(JSONObject response) {

    }

    /**
     * on Success of password chnage
     *
     * @return
     */
    private AlertDialogClickListener onDialogClick() {
        AlertDialogClickListener mListener = new AlertDialogClickListener() {

            @Override
            public void onClickOfAlertDialogPositive() {
                // TODO Auto-generated method stub

            }
        };
        return mListener;
    }
}
