package com.biblezon.support.global;

/**
 * Created by Anshuman on 12/16/2015.
 */
public interface GlobalHelper {

    /*API Links*/
    String BASE_API_PATH = "http://biblezon.com/appapicms/webservices/";
    String FEEDBACK_API = BASE_API_PATH + "feedbacks";
    String USER_MANUALS_API = BASE_API_PATH + "manuals";
    String FAQ_API = BASE_API_PATH + "faqs";
    String VIDEO_API = BASE_API_PATH + "youtubes";

    /*API request Key*/
    String FEEDBACK_KEY = "FEEDBACK_KEY";
    String FAQ_KEY = "FAQ_KEY";
    String USER_MANUALS_KEY = "USER_MANUALS_KEY";
    String VIDEO_KEY = "VIDEO_KEY";
    String DOWNLOAD_IMAGE = "DOWNLOAD_IMAGE";

    /*API Time out*/
    int ONE_SECOND = 1000;
    int API_REQUEST_TIME = 20;

    String ID = "id";
    String NAME = "name";
    String PHONE = "phone";
    String EMAIL = "email";
    String DESCRIPTION = "description";
    String TITLE = "title";
    String LINK = "link";

    /**
     * version check keys
     */
    String VERSION_CHECK_BASEURL = "http://biblezon.com/appapicms/webservices/autoupdate/";
    String VERSION_CHECK_KEY = "version_check";
    String MSG_SUCCESS = "success";
    String LATEST_VERSION = "latestVersion";
    String UPDATED_APP_URL = "appURI";
    String APK_NAME = "Support";


}
