package com.biblezon.support.webservice.control;


import com.biblezon.support.global.GlobalHelper;

import org.json.JSONObject;

/**
 * Webservice API Response is Success OR Fail
 *
 * @author Anshuman
 */
public class WebserviceAPISuccessFailManager {
    /**
     * Instance of This class
     */
    public static WebserviceAPISuccessFailManager mApiSuccessFailManager;
    /**
     * Debugging TAG
     */
    @SuppressWarnings("unused")
    private String TAG = WebserviceAPISuccessFailManager.class.getSimpleName();

    private WebserviceAPISuccessFailManager() {
    }

    /**
     * Get Instance of this class
     *
     * @return
     */
    public static WebserviceAPISuccessFailManager getInstance() {
        if (mApiSuccessFailManager == null)
            mApiSuccessFailManager = new WebserviceAPISuccessFailManager();
        return mApiSuccessFailManager;

    }

    /**
     * Check Response Code of the API
     *
     * @param mObject
     * @return
     */
    public boolean checkVersionResponseCode(JSONObject mObject) {
        try {
            boolean mResponseCode = mObject.getBoolean(GlobalHelper.MSG_SUCCESS);
            if (mResponseCode) {
                return true;
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            return false;
        }

        return false;
    }

    /**
     * Get Api Response Status
     *
     * @param response
     * @return
     */
    public boolean getReponseStatus(JSONObject response) {
//        if (response != null) {
//            try {
//                if (response.has(GlobalKeys.RESPONSE_STATUS)
//                        && response.getBoolean(GlobalKeys.RESPONSE_STATUS)) {
//                    return true;
//                } else {
//                    return false;
//                }
//            } catch (JSONException e) {
//                // e.printStackTrace();
//                return false;
//            }
//        }
        return false;
    }

    /**
     * Get Api Response Message
     *
     * @param response
     * @return
     */
    public String getReponseMessage(JSONObject response) {
//        if (response != null) {
//            try {
//                String msg = "";
//                if (response.has(GlobalKeys.RESPONSE_REASON)) {
//                    msg = response.getString(GlobalKeys.RESPONSE_REASON);
//                }
//                return msg;
//            } catch (JSONException e) {
//                e.printStackTrace();
//                return null;
//            }
//        }
        return null;
    }
    //
    // /**
    // * Get Api Response Data
    // *
    // * @param response
    // * @return
    // */
    // public JSONArray getReponseData(String response) {
    // if (response != null && !response.isEmpty()) {
    // JSONObject jsonObject;
    // try {
    // jsonObject = new JSONObject(response);
    // JSONArray data = jsonObject
    // .getJSONArray(GlobalKeys.RESPONSE_DATA);
    // return data;
    // } catch (JSONException e) {
    // e.printStackTrace();
    // return null;
    // }
    // }
    // return null;
    // }
    //
    // /**
    // * Get Api Response Data
    // *
    // * @param response
    // * @return
    // */
    // public JSONObject getReponseDataObject(String response) {
    // if (response != null && !response.isEmpty()) {
    // JSONObject jsonObject;
    // try {
    // jsonObject = new JSONObject(response);
    // JSONObject data = jsonObject
    // .getJSONObject(GlobalKeys.RESPONSE_DATA);
    // return data;
    // } catch (JSONException e) {
    // e.printStackTrace();
    // return null;
    // }
    // }
    // return null;
    // }
}
