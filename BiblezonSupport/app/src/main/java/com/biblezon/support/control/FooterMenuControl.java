package com.biblezon.support.control;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.biblezon.support.R;
import com.biblezon.support.view.FAQScreen;
import com.biblezon.support.view.FeedbackScreen;
import com.biblezon.support.view.SupportScreen;
import com.biblezon.support.view.UserManualScreen;
import com.biblezon.support.view.VideoScreen;


/**
 * Manage Footer Menu Control
 *
 * @author Anshuman
 */
public class FooterMenuControl implements OnClickListener {
    /**
     * Instance of Activity
     */
    private static Activity mActivity;
    /**
     * Manage Menu Slider layout view object
     */

    private LinearLayout videoTab, faqTab, feedbackTab, usermanualTab;
    private TextView videoText, faqText, feedbackText, usermanualText;
    private ImageView videoIcon, faqIcon, feedbackIcon, usermanualIcon;
    /**
     * Debugging TAG
     */
    @SuppressWarnings("unused")
    private String TAG = FooterMenuControl.class.getSimpleName();

    /**
     * Left Side Sliding Menu Control constructor
     */
    public FooterMenuControl(Activity mAct) {
        mActivity = mAct;
        initSlidingView();
        assignClicks();
    }

    /**
     * Manage Sliding View object
     */
    private void initSlidingView() {
        /*LinearLayout Id*/
        videoTab = (LinearLayout) mActivity.findViewById(R.id.videoTab);
        faqTab = (LinearLayout) mActivity.findViewById(R.id.faqTab);
        feedbackTab = (LinearLayout) mActivity.findViewById(R.id.feedbackTab);
        usermanualTab = (LinearLayout) mActivity
                .findViewById(R.id.usermanualTab);
        /*Text Views Id*/
        videoText = (TextView) mActivity.findViewById(R.id.videoText);
        faqText = (TextView) mActivity.findViewById(R.id.faqText);
        feedbackText = (TextView) mActivity.findViewById(R.id.feedbackText);
        usermanualText = (TextView) mActivity.findViewById(R.id.usermanualText);
/*Image Views Id*/
        videoIcon = (ImageView) mActivity.findViewById(R.id.videoIcon);
        faqIcon = (ImageView) mActivity.findViewById(R.id.faqIcon);
        feedbackIcon = (ImageView) mActivity.findViewById(R.id.feedbackIcon);
        usermanualIcon = (ImageView) mActivity.findViewById(R.id.usermanualIcon);

    }

    /**
     * Assign click to View objects
     */
    private void assignClicks() {
        videoTab.setOnClickListener(this);
        faqTab.setOnClickListener(this);
        feedbackTab.setOnClickListener(this);
        usermanualTab.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.videoTab:
                onVideoTabSelected();
                break;
            case R.id.faqTab:
                onFAQTabSelected();
                break;
            case R.id.feedbackTab:
                onFeedbackTabSelected();
                break;
            case R.id.usermanualTab:
                onUserManualTabSelected();
                break;

            default:

                break;
        }

    }

    /**
     * On Video Tab Selected
     */
    public void onVideoTabSelected() {
        /*Update Text Color*/
        videoText.setTextColor(mActivity.getResources().getColor(R.color.colorOrangeDark));
        faqText.setTextColor(mActivity.getResources().getColor(R.color.colorGrayLight));
        feedbackText.setTextColor(mActivity.getResources().getColor(R.color.colorGrayLight));
        usermanualText.setTextColor(mActivity.getResources().getColor(R.color.colorGrayLight));
        /*Update  Icon*/
        videoIcon.setImageResource(R.drawable.video_selected);
        faqIcon.setImageResource(R.drawable.faq_unselected);
        feedbackIcon.setImageResource(R.drawable.feedback_unselected);
        usermanualIcon.setImageResource(R.drawable.user_unselected);

        /*Update Fragment*/
        SupportScreen.getInstance().pushFragment(new VideoScreen());
    }

    /**
     * On FAQ Tab Selected
     */
    private void onFAQTabSelected() {
  /*Update Text Color*/
        videoText.setTextColor(mActivity.getResources().getColor(R.color.colorGrayLight));
        faqText.setTextColor(mActivity.getResources().getColor(R.color.colorOrangeDark));
        feedbackText.setTextColor(mActivity.getResources().getColor(R.color.colorGrayLight));
        usermanualText.setTextColor(mActivity.getResources().getColor(R.color.colorGrayLight));
        /*Update  Icon*/
        videoIcon.setImageResource(R.drawable.video_unselected);
        faqIcon.setImageResource(R.drawable.faq_selected);
        feedbackIcon.setImageResource(R.drawable.feedback_unselected);
        usermanualIcon.setImageResource(R.drawable.user_unselected);

          /*Update Fragment*/
        SupportScreen.getInstance().pushFragment(new FAQScreen());
    }

    /**
     * On Feedback Tab Selected
     */
    private void onFeedbackTabSelected() {
 /*Update Text Color*/
        videoText.setTextColor(mActivity.getResources().getColor(R.color.colorGrayLight));
        faqText.setTextColor(mActivity.getResources().getColor(R.color.colorGrayLight));
        feedbackText.setTextColor(mActivity.getResources().getColor(R.color.colorOrangeDark));
        usermanualText.setTextColor(mActivity.getResources().getColor(R.color.colorGrayLight));
        /*Update  Icon*/
        videoIcon.setImageResource(R.drawable.video_unselected);
        faqIcon.setImageResource(R.drawable.faq_unselected);
        feedbackIcon.setImageResource(R.drawable.feedback_selected);
        usermanualIcon.setImageResource(R.drawable.user_unselected);
          /*Update Fragment*/
        SupportScreen.getInstance().pushFragment(new FeedbackScreen());
    }

    /**
     * On UserManual Tab Selected
     */
    private void onUserManualTabSelected() {
/*Update Text Color*/
        videoText.setTextColor(mActivity.getResources().getColor(R.color.colorGrayLight));
        faqText.setTextColor(mActivity.getResources().getColor(R.color.colorGrayLight));
        feedbackText.setTextColor(mActivity.getResources().getColor(R.color.colorGrayLight));
        usermanualText.setTextColor(mActivity.getResources().getColor(R.color.colorOrangeDark));
        /*Update  Icon*/
        videoIcon.setImageResource(R.drawable.video_unselected);
        faqIcon.setImageResource(R.drawable.faq_unselected);
        feedbackIcon.setImageResource(R.drawable.feedback_unselected);
        usermanualIcon.setImageResource(R.drawable.user_selected);

          /*Update Fragment*/
        SupportScreen.getInstance().pushFragment(new UserManualScreen());
    }


}
