package com.biblezon.support.view;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.biblezon.support.R;
import com.biblezon.support.Rest.CheckLatestVersionAsynctask;
import com.biblezon.support.control.FooterMenuControl;
import com.biblezon.support.ihelper.ChoiceDialogClickListener;
import com.biblezon.support.utils.AppDialogUtils;

import java.util.Stack;

/**
 * Created by Anshuman on 12/11/2015.
 */
public class SupportScreen extends FragmentActivity {
    /*SupportScreen Instance*/
    private static SupportScreen mSupportScreen;
    /*Activity Instance*/
    private Activity mActivity;
    /*Footer Menu Control*/
    private FooterMenuControl mFooterMenuControl;
    /*Debugging TAG*/
    private String TAG = SupportScreen.class.getSimpleName();
    /**
     * Fragment showing and opening Stack
     */
    private Stack<Fragment> mFragmentStack = new Stack<Fragment>();

    /**
     * Get Current Screen Instance
     *
     * @return
     */
    public static SupportScreen getInstance() {
        if (mSupportScreen != null)
            return mSupportScreen;
        else
            return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.support_screen);
        initViews();
        new CheckLatestVersionAsynctask(this).execute();
    }

    /**
     * Init Views of application
     */
    private void initViews() {
        mActivity = this;
        mSupportScreen = this;
        /*Footer Control*/
        mFooterMenuControl = new FooterMenuControl(mActivity);

        /*Add first base fragment*/
        mFooterMenuControl.onVideoTabSelected();
    }

    /**
     * Push Fragment into fragment Stack and show top fragment to GUI
     */
    public void pushFragment(Fragment mFragment) {
        try {
            manageFragmentStack(mFragment);
            // Begin the transaction
            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction();
            // add fragment on frame-layout
            transaction.add(R.id.containerView, mFragment);
            transaction.addToBackStack(null);
            // Commit the transaction
            transaction.commitAllowingStateLoss();
            // add fragment into flow stack
            mFragmentStack.add(mFragment);

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    /**
     * Manage fragment stack
     */
    public void manageFragmentStack(Fragment mCurrentFragment) {
        if (mCurrentFragment instanceof VideoScreen
                || mCurrentFragment instanceof FeedbackScreen
                || mCurrentFragment instanceof FAQScreen
                || mCurrentFragment instanceof UserManualScreen) {
            removeAllOldFragment();
        }
    }

    /**
     * Pop Fragment from fragment stack and remove latest one and show Pervious
     * attached fragment
     */
    public void popFragment(Fragment mFragment) {
        try {
            // Begin the transaction
            FragmentTransaction ft = getSupportFragmentManager()
                    .beginTransaction();
            // remove fragment from view
            if (mFragment != null) {
                ft.remove(mFragment);
                ft.detach(mFragment);
            }
            ft.commit();
            getSupportFragmentManager().popBackStack();
            mFragmentStack.remove(mFragmentStack.lastElement());
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    /**
     * Remove all attached old fragments
     */
    public void removeAllOldFragment() {
        try {
            for (int i = 0; i < mFragmentStack.size(); i++) {
                popFragment(mFragmentStack.get(i));
            }
            mFragmentStack.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        if (mFragmentStack.lastElement() instanceof VideoScreen
                || mFragmentStack.lastElement() instanceof FeedbackScreen
                || mFragmentStack.lastElement() instanceof FAQScreen
                || mFragmentStack.lastElement() instanceof UserManualScreen) {
            AppDialogUtils.showChoiceDialog(this, this.getResources()
                            .getString(R.string.exit_from_app), this.getResources()
                            .getString(R.string.yes),
                    this.getResources().getString(R.string.no),
                    AppExitManager());
        } else {

            super.onBackPressed();
            mFragmentStack.pop();
//            manageSlideMenuAccordingToFragment(mFragmentStack.lastElement());

        }
    }

    /**
     * Exit From App Process
     *
     * @return
     */
    private ChoiceDialogClickListener AppExitManager() {
        ChoiceDialogClickListener mClickListener = new ChoiceDialogClickListener() {

            @Override
            public void onClickOfPositive() {
                finish();
            }

            @Override
            public void onClickOfNegative() {

            }
        };

        return mClickListener;
    }



    // broadcast receiver to get notification about ongoing downloads
    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            // check if the broadcast message is for our Enqueued download
            long referenceId = intent.getLongExtra(
                    DownloadManager.EXTRA_DOWNLOAD_ID, -1);
//            AndroidAppUtils.showLog(TAG,
//                    "downloadReference *******  referenceId : "
//                            + downloadReference + " ******** " + referenceId);
            if (downloadReference == referenceId) {

//                AndroidAppUtils.showVerboseLog("VersionCheckAPIHandler",
//                        "Downloading of the new app version complete");
                // start the installation of the latest version
                Intent installIntent = new Intent(Intent.ACTION_VIEW);
                installIntent.setDataAndType(downloadManager
                                .getUriForDownloadedFile(downloadReference),
                        "application/vnd.android.package-archive");
                installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(installIntent);

            }
        }
    };


    //----------------------- auto update code start ----------------------------------------------//

    DownloadManager downloadManager;
    private long downloadReference;

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        IntentFilter filter = new IntentFilter(
                DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(downloadReceiver, filter);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        try {
            if (downloadReceiver != null) {
                unregisterReceiver(downloadReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //----------------------- auto update code end ------------------------------------------------//
}
