package com.biblezon.support.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.biblezon.support.R;
import com.biblezon.support.model.VideoModel;
import com.biblezon.support.utils.AndroidAppUtils;
import com.biblezon.support.webservice.GetImageAPIHandler;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * show Videos list GUI on GUI and manage its click and view
 *
 * @author Anshuman
 */
public class VideoListAdapter extends BaseAdapter {


    Activity mActivity;
    private ArrayList<VideoModel> mVideoList;
    /**
     * Context object
     */
    private Context context;
    @SuppressWarnings("unused")
    private LayoutInflater mLayoutInflater;
    /**
     * Debugging TAG
     */
    @SuppressWarnings("unused")
    private String TAG = VideoListAdapter.class.getSimpleName();

    /**
     * Basic constructor of class
     *
     * @param activity
     */
    public VideoListAdapter(Activity activity) {
        this.context = activity;
        this.mActivity = activity;
        try {
            mLayoutInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            mVideoList = new ArrayList<VideoModel>();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addUpdatedData(
            ArrayList<VideoModel> mVideoListData) {
        mVideoList = new ArrayList<VideoModel>();
        mVideoList = mVideoListData;
    }

    @Override
    public int getCount() {
        if (mVideoList != null) {
            return mVideoList.size();
        } else {
            return 0;
        }

    }

    @Override
    public VideoModel getItem(int position) {
        if (mVideoList != null) {
            return mVideoList.get(position);
        } else {
            return null;
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context.getApplicationContext(),
                    R.layout.video_row, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        if (mVideoList != null && mVideoList.size() > position) {
            holder.videoNameText.setText(mVideoList.get(position).getTitle());
            String image_Path = "http://img.youtube.com/vi/" + AndroidAppUtils.getVideoId(mVideoList.get(position).getLink()) + "/0.jpg";
//            AndroidAppUtils.setImageonGUI(image_Path, holder.youtubtumb);
            new GetImageAPIHandler(mActivity, image_Path, null, position, holder.youtubtumb);
        } else
            AndroidAppUtils.showErrorLog(TAG, " mVideoList is empty");
        return convertView;
    }

    /**
     * List view row object and its views
     *
     * @author Anshuman
     */
    class ViewHolder {

        RelativeLayout baseVideoRow;
        TextView videoNameText;
        ImageView youtubtumb;

        public ViewHolder(View view) {
            baseVideoRow = (RelativeLayout) view.findViewById(R.id.baseVideoRow);
            videoNameText = (TextView) view.findViewById(R.id.videoNameText);
            youtubtumb = (ImageView) view.findViewById(R.id.youtubtumb);
            view.setTag(this);
        }
    }
}
