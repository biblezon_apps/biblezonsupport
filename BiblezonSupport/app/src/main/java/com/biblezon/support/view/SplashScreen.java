package com.biblezon.support.view;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.biblezon.support.R;
import com.biblezon.support.global.GlobalHelper;
import com.biblezon.support.utils.AndroidAppUtils;
import com.biblezon.support.webservice.SupportAPIHandler;
import com.biblezon.support.webservice.VersionCheckAPIHandler;
import com.biblezon.support.webservice.ihelper.WebAPIResponseListener;

/**
 * Created by Anshuman on 12/16/2015.
 */
public class SplashScreen extends Activity {
    String TAG = SplashScreen.class.getSimpleName();
    TextView support, bible;
    Activity mActivity;
    Animation slide_in_right, slide_in_left;
    private LinearLayout processView, retryView;

    SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        initViews();
        assignClick();
        startAnimation();
        prefs = PreferenceManager.getDefaultSharedPreferences(SplashScreen.this);

        new VersionCheckAPIHandler(mActivity, webAPIResponseLinsener());
    }

    /**
     * Mass Base Response Listener
     */
    private WebAPIResponseListener webAPIResponseLinsener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessResponse(Object... arguments) {
                new SupportAPIHandler(mActivity, APIResponseLinsener("VIDEO"), false, GlobalHelper.VIDEO_API);
            }

            @Override
            public void onFailResponse(Object... arguments) {
                new SupportAPIHandler(mActivity, APIResponseLinsener("VIDEO"), false, GlobalHelper.VIDEO_API);
            }

            @Override
            public void onOfflineResponse(Object... arguments) {
                new SupportAPIHandler(mActivity, APIResponseLinsener("VIDEO"), false, GlobalHelper.VIDEO_API);
            }
        };
        return mListener;
    }

    private void assignClick() {
        retryView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processView.setVisibility(View.VISIBLE);
                retryView.setVisibility(View.GONE);
                new SupportAPIHandler(mActivity, APIResponseLinsener("VIDEO"), false, GlobalHelper.VIDEO_API);
            }
        });
    }

    private void initViews() {
        mActivity = this;
        support = (TextView) findViewById(R.id.support);
        bible = (TextView) findViewById(R.id.bible);
        processView = (LinearLayout) findViewById(R.id.processView);
        retryView = (LinearLayout) findViewById(R.id.retryView);
        retryView.setVisibility(View.GONE);
        support.setVisibility(View.GONE);
        bible.setVisibility(View.GONE);
        slide_in_right = AnimationUtils.loadAnimation(mActivity,
                R.anim.slide_in_right);
        slide_in_left = AnimationUtils.loadAnimation(mActivity,
                R.anim.slide_in_left);
    }


    private void startAnimation() {
        support.setVisibility(View.VISIBLE);
        bible.setVisibility(View.VISIBLE);
        support.startAnimation(slide_in_left);
        bible.startAnimation(slide_in_right);
    }

    /**
     * All API Response Listener
     *
     * @return
     */
    private WebAPIResponseListener APIResponseLinsener(final String reqType) {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {
            @Override
            public void onSuccessResponse(Object... arguments) {
                try {
                    if (reqType.equalsIgnoreCase("VIDEO")) {
                        new SupportAPIHandler(mActivity, APIResponseLinsener("FAQ"), false, GlobalHelper.FAQ_API);
                    } else if (reqType.equalsIgnoreCase("FAQ")) {
                        new SupportAPIHandler(mActivity, APIResponseLinsener("MANUALS"), false, GlobalHelper.USER_MANUALS_API);
                    } else if (reqType.equalsIgnoreCase("MANUALS")) {

                        if(prefs.getBoolean("Is_Login", false)){

                            Intent myIntent = new Intent(SplashScreen.this, SupportScreen.class);
                            startActivity(myIntent);
                            finish();

                        }else
                        {

                            Intent myIntent = new Intent(SplashScreen.this, LoginActivity.class);
                            startActivity(myIntent);
                            finish();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailResponse(Object... arguments) {
                AndroidAppUtils.showToast(mActivity, mActivity.getResources().getString(
                        R.string.network_error));
                processView.setVisibility(View.GONE);
                retryView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onOfflineResponse(Object... arguments) {
                AndroidAppUtils.showToast(mActivity, mActivity.getResources().getString(
                        R.string.network_error));
                processView.setVisibility(View.GONE);
                retryView.setVisibility(View.VISIBLE);
            }
        };
        return mListener;
    }
}
