package com.biblezon.support.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.biblezon.support.R;
import com.biblezon.support.model.UserManualsModel;
import com.biblezon.support.utils.AndroidAppUtils;

import java.util.ArrayList;

/**
 * show Videos list GUI on GUI and manage its click and view
 *
 * @author Anshuman
 */
public class UserManualListAdapter extends BaseAdapter {

    Activity mActivity;
    private ArrayList<UserManualsModel> mUserManualsList;
    /**
     * Context object
     */
    private Context context;
    @SuppressWarnings("unused")
    private LayoutInflater mLayoutInflater;
    /**
     * Debugging TAG
     */
    @SuppressWarnings("unused")
    private String TAG = UserManualListAdapter.class.getSimpleName();

    /**
     * Basic constructor of class
     *
     * @param activity
     */
    public UserManualListAdapter(Activity activity) {
        this.context = activity;
        this.mActivity = activity;
        try {
            mLayoutInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            mUserManualsList = new ArrayList<UserManualsModel>();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addUpdatedData(
            ArrayList<UserManualsModel> mData) {
        mUserManualsList = new ArrayList<UserManualsModel>();
        this.mUserManualsList = mData;
    }

    @Override
    public int getCount() {
        if (mUserManualsList != null) {
            return mUserManualsList.size();
        } else {
            return 0;
        }

    }

    @Override
    public UserManualsModel getItem(int position) {
        if (mUserManualsList != null) {
            return mUserManualsList.get(position);
        } else {
            return null;
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context.getApplicationContext(),
                    R.layout.user_manual_row, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        if (mUserManualsList != null && mUserManualsList.size() > position) {
            holder.userManualDeatilsText.setText(Html.fromHtml(mUserManualsList.get(position).getDescription()));
        } else
            AndroidAppUtils.showErrorLog(TAG, "User manual List is empty");
        return convertView;
    }

    /**
     * List view row object and its views
     *
     * @author Anshuman
     */
    class ViewHolder {

        TextView userManualText, userManualDeatilsText;

        public ViewHolder(View view) {
            userManualText = (TextView) view.findViewById(R.id.userManualText);
            userManualDeatilsText = (TextView) view.findViewById(R.id.userManualDeatilsText);
            view.setTag(this);
        }
    }
}
