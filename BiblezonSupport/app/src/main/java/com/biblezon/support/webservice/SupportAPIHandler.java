package com.biblezon.support.webservice;

import android.annotation.SuppressLint;
import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.support.R;
import com.biblezon.support.application.ApplicationController;
import com.biblezon.support.global.GlobalHelper;
import com.biblezon.support.model.FAQModel;
import com.biblezon.support.model.FeedbackModel;
import com.biblezon.support.model.UserManualsModel;
import com.biblezon.support.model.VideoModel;
import com.biblezon.support.utils.AndroidAppUtils;
import com.biblezon.support.webservice.control.WebserviceAPIErrorHandler;
import com.biblezon.support.webservice.ihelper.WebAPIResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * SupportAPIHandler API Handler
 *
 * @author Anshuman
 */
public class SupportAPIHandler {
    public static ArrayList<FAQModel> mListOfFAQ = new ArrayList<>();
    public static ArrayList<FeedbackModel> mListOfFeedback = new ArrayList<>();
    public static ArrayList<UserManualsModel> mListOfUserManuals = new ArrayList<>();
    public static ArrayList<VideoModel> mListOfVideos = new ArrayList<>();
    /**
     * Instance object of Login API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = SupportAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    /**
     * Url paths
     */
    private String URL_PATH = "";

    /**
     * Basic Constructor of this class
     *
     * @param mActivity
     * @param webAPIResponseListener
     * @param mActivity
     */

    public SupportAPIHandler(Activity mActivity,
                             WebAPIResponseListener webAPIResponseListener,
                             boolean isProgressShowing, String URL) {
        if (isProgressShowing) {
            AndroidAppUtils.showProgressDialog(mActivity,
                    mActivity.getString(R.string.loading), false);
        }
        this.mActivity = mActivity;
        this.URL_PATH = URL;
        this.mResponseListener = webAPIResponseListener;
        postAPICall();

    }

    /**
     * Making json object request
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        AndroidAppUtils.showLog(TAG, "URL :" + URL_PATH);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                (URL_PATH).trim(), null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        AndroidAppUtils.showInfoLog(TAG, "Response :"
                                + response);
                        parseAPIResponse(response);

                        AndroidAppUtils.hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                AndroidAppUtils.showErrorLog(
                        TAG,
                        WebserviceAPIErrorHandler.getInstance()
                                .VolleyErrorHandlerReturningString(
                                        error, mActivity));
                mResponseListener.onOfflineResponse();
                AndroidAppUtils.hideProgressDialog();

            }
        });
        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(jsonObjReq,
                GlobalHelper.VIDEO_KEY);
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                GlobalHelper.ONE_SECOND * GlobalHelper.API_REQUEST_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // MassAppApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.GET_COMPANY_NAME_KEY);
    }

    /**
     * Parse API Response
     *
     * @param response
     */
    @SuppressLint("NewApi")
    protected void parseAPIResponse(JSONObject response) {
        String responseType = "";
        try {
            if (response.has("replyCode")) {
                if (response.getString("replyCode").equalsIgnoreCase("success")) {
/*Check Which Type Of data is it*/
                    if (response.has("replyMsg")) {
                        responseType = response.getString("replyMsg");
                    }
                }
            }

            if (responseType.equalsIgnoreCase("Support Video found")) {
                /*Video Data*/
                VideoDataParser(response);
            } else if (responseType.equalsIgnoreCase("Feedback found")) {
                /*Feedback Data*/
                FeedbackDataParser(response);
            } else if (responseType.equalsIgnoreCase("Faq found")) {
                /*FAQ Data*/
                FAQDataParser(response);
            } else if (responseType.equalsIgnoreCase("User Manual found")) {
                /*User manual Data*/
                UserManualsDataParser(response);
            } else {
                AndroidAppUtils.showErrorLog(TAG, "Not Data for this app");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Parse Video Data
     */
    private void VideoDataParser(JSONObject response) {
        mListOfVideos = new ArrayList<>();
        try {
            JSONArray mJsonArray = response.getJSONArray("data");
            for (int i = 0; i < mJsonArray.length(); i++) {
                JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                if (mJsonObject != null) {
                    VideoModel videoModel = new VideoModel();
                    if (mJsonObject.has(GlobalHelper.ID)) {
                        videoModel.setId(mJsonObject.getString(GlobalHelper.ID));
                    }
                    if (mJsonObject.has(GlobalHelper.TITLE)) {
                        videoModel.setTitle(mJsonObject.getString(GlobalHelper.TITLE));
                    }
                    if (mJsonObject.has(GlobalHelper.LINK)) {
                        videoModel.setLink(mJsonObject.getString(GlobalHelper.LINK));
                    }
                    mListOfVideos.add(videoModel);

                }
            }
            mResponseListener.onSuccessResponse();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Feedback  Data
     *
     * @param response
     */
    private void FeedbackDataParser(JSONObject response) {
        mListOfFeedback = new ArrayList<>();
        try {
            JSONArray mJsonArray = response.getJSONArray("data");
            for (int i = 0; i < mJsonArray.length(); i++) {
                JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                if (mJsonObject != null) {
                    FeedbackModel feedbackModel = new FeedbackModel();
                    if (mJsonObject.has(GlobalHelper.ID)) {
                        feedbackModel.setId(mJsonObject.getString(GlobalHelper.ID));
                    }
                    if (mJsonObject.has(GlobalHelper.NAME)) {
                        feedbackModel.setName(mJsonObject.getString(GlobalHelper.NAME));
                    }
                    if (mJsonObject.has(GlobalHelper.DESCRIPTION)) {
                        feedbackModel.setDescription(mJsonObject.getString(GlobalHelper.DESCRIPTION));
                    }
                    if (mJsonObject.has(GlobalHelper.EMAIL)) {
                        feedbackModel.setEmail(mJsonObject.getString(GlobalHelper.EMAIL));
                    }
                    if (mJsonObject.has(GlobalHelper.PHONE)) {
                        feedbackModel.setPhone(mJsonObject.getString(GlobalHelper.PHONE));
                    }
                    mListOfFeedback.add(feedbackModel);
                }

            }
            mResponseListener.onSuccessResponse();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * FAQ  Data
     */
    private void FAQDataParser(JSONObject response) {
        mListOfFAQ = new ArrayList<>();
        try {
            JSONArray mJsonArray = response.getJSONArray("data");
            for (int i = 0; i < mJsonArray.length(); i++) {
                JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                if (mJsonObject != null) {
                    FAQModel faqmodel = new FAQModel();
                    if (mJsonObject.has(GlobalHelper.ID)) {
                        faqmodel.setId(mJsonObject.getString(GlobalHelper.ID));
                    }
                    if (mJsonObject.has(GlobalHelper.DESCRIPTION)) {
                        faqmodel.setDescription(mJsonObject.getString(GlobalHelper.DESCRIPTION));
                    }
                    mListOfFAQ.add(faqmodel);
                }
            }
            mResponseListener.onSuccessResponse();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * User Manuals Data
     */
    private void UserManualsDataParser(JSONObject response) {
        mListOfUserManuals = new ArrayList<>();
        try {
            JSONArray mJsonArray = response.getJSONArray("data");
            for (int i = 0; i < mJsonArray.length(); i++) {
                JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                if (mJsonObject != null) {
                    UserManualsModel usermanualsmodel = new UserManualsModel();
                    if (mJsonObject.has(GlobalHelper.ID)) {
                        usermanualsmodel.setId(mJsonObject.getString(GlobalHelper.ID));
                    }
                    if (mJsonObject.has(GlobalHelper.DESCRIPTION)) {
                        usermanualsmodel.setDescription(mJsonObject.getString(GlobalHelper.DESCRIPTION));
                    }
                    mListOfUserManuals.add(usermanualsmodel);
                }
            }
            mResponseListener.onSuccessResponse();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
