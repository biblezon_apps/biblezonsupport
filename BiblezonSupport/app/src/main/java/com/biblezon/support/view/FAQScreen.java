package com.biblezon.support.view;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.biblezon.support.R;
import com.biblezon.support.webservice.SupportAPIHandler;


/**
 * Slider fragment Help Screen
 *
 * @author Anshuman
 */
public class FAQScreen extends Fragment {

    /*List adapter*/
//    FAQListAdapter mFaqListAdapter;
    /**
     * View fields instances
     */
    @SuppressWarnings("unused")
    private String TAG = FAQScreen.class.getSimpleName();
    private Activity mActivity;
    /**
     * Screen base view
     */
    private View mView;
    /**
     * Screen Views
     */
//    private ListView faqlist;
    private TextView headerName;
    private WebView faqWebView;

    /*
     * (non-Javadoc)
     *
     * @see
     * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.faq_screen, container, false);
        initViews();
        return mView;
    }

    /**
     * initializing view fields
     */
    private void initViews() {
        mActivity = getActivity();
//        faqlist = (ListView) mView.findViewById(R.id.faqlist);
        headerName = (TextView) mView.findViewById(R.id.headerName);
        headerName.setText(mActivity.getResources().getString(R.string.faq));
/*List Adapter*/
//        mFaqListAdapter = new FAQListAdapter(mActivity);
//        faqlist.setAdapter(mFaqListAdapter);
//        mFaqListAdapter.addUpdatedData(SupportAPIHandler.mListOfFAQ);
//        mFaqListAdapter.notifyDataSetChanged();


        faqWebView = (WebView) mView.findViewById(R.id.faqWebView);
        faqWebView.setBackgroundColor(Color.TRANSPARENT);
        faqWebView.loadDataWithBaseURL("", "<font color='black'>" + "<font size='4'>"
                + SupportAPIHandler.mListOfFAQ.get(0).getDescription()
                + "</font>", "text/html", "UTF-8", "");

//        faqWebView.loadUrl("http://www.biblezon.com/faq/#faq");
    }


}
