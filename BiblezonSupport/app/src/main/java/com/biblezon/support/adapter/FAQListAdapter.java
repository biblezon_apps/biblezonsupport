package com.biblezon.support.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.biblezon.support.R;
import com.biblezon.support.model.FAQModel;
import com.biblezon.support.utils.AndroidAppUtils;

import java.util.ArrayList;

/**
 * show Videos list GUI on GUI and manage its click and view
 *
 * @author Anshuman
 */
public class FAQListAdapter extends BaseAdapter {

    Activity mActivity;
    private ArrayList<FAQModel> mFaqList;
    /**
     * Context object
     */
    private Context context;
    @SuppressWarnings("unused")
    private LayoutInflater mLayoutInflater;
    /**
     * Debugging TAG
     */
    @SuppressWarnings("unused")
    private String TAG = FAQListAdapter.class.getSimpleName();

    /**
     * Basic constructor of class
     *
     * @param activity
     */
    public FAQListAdapter(Activity activity) {
        this.context = activity;
        this.mActivity = activity;
        try {
            mLayoutInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            mFaqList = new ArrayList<FAQModel>();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addUpdatedData(
            ArrayList<FAQModel> mFAQListData) {
        mFaqList = new ArrayList<FAQModel>();
        mFaqList = mFAQListData;
    }

    @Override
    public int getCount() {
        if (mFaqList != null) {
            return mFaqList.size();
        } else {
            return 0;
        }

    }

    @Override
    public FAQModel getItem(int position) {
        if (mFaqList != null) {
            return mFaqList.get(position);
        } else {
            return null;
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context.getApplicationContext(),
                    R.layout.faq_row, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        if (mFaqList != null && mFaqList.size() > position) {
            holder.faqdeatilsText.setText(Html.fromHtml(mFaqList.get(position).getDescription()));
        } else
            AndroidAppUtils.showErrorLog(TAG, "FAQ List is empty");
        return convertView;
    }

    /**
     * List view row object and its views
     *
     * @author Anshuman
     */
    class ViewHolder {

        TextView faqheadingText, faqdeatilsText;

        public ViewHolder(View view) {
            faqheadingText = (TextView) view.findViewById(R.id.faqheadingText);
            faqdeatilsText = (TextView) view.findViewById(R.id.faqdeatilsText);
            view.setTag(this);
        }
    }
}
