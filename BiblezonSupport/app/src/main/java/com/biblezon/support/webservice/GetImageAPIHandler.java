package com.biblezon.support.webservice;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.biblezon.support.application.ApplicationController;
import com.biblezon.support.global.GlobalHelper;
import com.biblezon.support.utils.AndroidAppUtils;
import com.biblezon.support.webservice.ihelper.WebAPIResponseListener;

/**
 * Get user image API Handler
 *
 * @author Shruti
 */
public class GetImageAPIHandler {
    /**
     * Instance object of get image API
     */
    @SuppressWarnings("unused")
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = GetImageAPIHandler.class.getSimpleName();
    /**
     * Request Data
     */
    private String image_path;
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    private int position = 0;
    private ImageView videThubView;

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public GetImageAPIHandler(Activity mActivity, String image_path,
                              WebAPIResponseListener webAPIResponseListener,
                              int position, ImageView videThubView) {

        this.mActivity = mActivity;
        this.image_path = image_path;
        this.position = position;
        this.videThubView = videThubView;
        this.mResponseListener = webAPIResponseListener;
        postAPICall();
    }

    /**
     * Call API to Get User image
     */
    private void postAPICall() {
        @SuppressWarnings("deprecation")
        ImageRequest mImageRequest = new ImageRequest(image_path.trim(),
                new Listener<Bitmap>() {

                    @Override
                    public void onResponse(Bitmap bitmap) {
                        if (bitmap != null) {
                            try {
                                AndroidAppUtils.showLog(TAG,
                                        "Success To get user image");
                                BitmapDrawable ob = new BitmapDrawable(mActivity.getResources(), bitmap);
                                videThubView.setBackgroundDrawable(ob);
                                if (mResponseListener != null)
                                    mResponseListener.onSuccessResponse();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            AndroidAppUtils.showLog(TAG, "This is not a profile pic request");
                            if (mResponseListener != null)
                                mResponseListener.onSuccessResponse(bitmap);
                        }
                    }
                }, 0, 0, null, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                AndroidAppUtils.showLog(TAG,
                        "get image onErrorResponse " + error);
                if (mResponseListener != null)
                    mResponseListener.onOfflineResponse();
            }
        }) {

        };
        // Adding request to request queue
        ApplicationController.getInstance().addToImageRequestQueue(
                mImageRequest, GlobalHelper.DOWNLOAD_IMAGE + position);
        // set request time-out
        mImageRequest.setRetryPolicy(new DefaultRetryPolicy(
                GlobalHelper.ONE_SECOND * GlobalHelper.API_REQUEST_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // ReDriverApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.LOGIN_REQUEST_KEY);
    }

}