package com.biblezon.support.view;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.biblezon.support.R;

/**
 * Created by Anshuman on 12/17/2015.
 */
public class VideoPlayer extends Activity {

    /*Debugging tag*/
    private String TAG = VideoPlayer.class.getSimpleName();
    /*Screens view ids*/
    private WebView videoPlayer;
    private TextView headerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_player_screen);

        initviews();
    }

    private void initviews() {

        headerName = (TextView) findViewById(R.id.headerName);
        headerName.setText(VideoScreen.videoName);

        videoPlayer = (WebView) findViewById(R.id.videoPlayer);
        videoPlayer.getSettings().setJavaScriptEnabled(true);
        videoPlayer.getSettings().setPluginState(WebSettings.PluginState.ON);
        videoPlayer.loadUrl(VideoScreen.youTub_URL);
//        videoPlayer.setWebChromeClient(new WebChromeClient());
    }
}
