package com.biblezon.support.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.biblezon.support.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView headerName;
    private android.widget.EditText emailEditText;
    private android.widget.EditText passwordEditText;
    private TextView forgotpasswordTextView;
    private android.widget.Button loginButton;
    private TextView newaccountTextView;

    private String LOGIN_URL ="http://biblezon.com/appapicms/webservices/support_login";

    //auto login
    SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.newaccountTextView = (TextView) findViewById(R.id.newaccountTextView);
        this.loginButton = (Button) findViewById(R.id.loginButton);
        this.forgotpasswordTextView = (TextView) findViewById(R.id.forgotpasswordTextView);
        this.passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        this.emailEditText = (EditText) findViewById(R.id.emailEditText);
        headerName = (TextView) findViewById(R.id.headerName);
        headerName.setText(R.string.Login);

        prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);

        loginButton.setOnClickListener(this);
        forgotpasswordTextView.setOnClickListener(this);
        newaccountTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()){

            case R.id.loginButton:
                String username = emailEditText.getText().toString().trim();
                String password = passwordEditText.getText().toString().trim();
                if (!TextUtils.isEmpty(username)&&!TextUtils.isEmpty(password)){
                    signup(username,password);
                }

                break;
            case R.id.forgotpasswordTextView:
//                startActivity(new Intent(LoginActivity.this,ForgortPasswordActivity.class));
                break;
            case R.id.newaccountTextView:
                startActivity(new Intent(LoginActivity.this,SignupActivity.class));
                break;

        }

    }



    private void signup(final String username, final String password) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            String isSuccess = jsonObject.optString("replycode");
                            String replymsg = jsonObject.optString("replymsg");
                            if (isSuccess.equals("200")) {

                                prefs.edit().putBoolean("Is_Login", true).apply();

                                Toast.makeText(LoginActivity.this, replymsg, Toast.LENGTH_LONG).show();

                                startActivity(new Intent(LoginActivity.this,SupportScreen.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                String userToken = jsonObject.optString("token");
                                String userEmail = jsonObject.optString("email");
                                String userName = jsonObject.optString("name");

                                prefs.edit().putString("token", userToken).apply();
                                prefs.edit().putString("email",userEmail ).apply();
                                prefs.edit().putString("name",userName ).apply();

                                Log.e("UserToken", userToken);
                                Log.e("UserEmail", userEmail);
                                Log.e("UserName", userName);


                            } else {

                                Toast.makeText(LoginActivity.this, replymsg, Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("email", username);
                map.put("password", password);
                map.put("device_token", "");
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}
